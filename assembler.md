# assembler

# hola mundo

```
section .data
    msg db "Hello world!", 0ah

section .text
    global _start

_start:
    mov rax, 1
    mov rdi, 1
    mov rsi, msg
    mov rdx, 13
    
    syscall
    mov rax, 60
    mov rdi, 0
    syscall
```

# obtener el tamño 

```
section .data
    msg db "Hello world!", 0ah
    len equ $ - msg
...

    mov rsi, msg
    mov rdx, len
...

```

# imprimir numero de un digito

```
section .data
    msg db "numero = #", 0ah
    len equ $ - msg

...

_start:

    mov rax, 3
    add rax, 48
    mov [msg +9], rax

...

```
# sumar 

```
_start:
    mov rax, 3
    mov rbx, 2
    add rax, rbx
...

```

# restar

```
_start:
    mov rax, 3
    mov rbx, 2
    sub rax, rbx
...

```

# multiplicar

```
_start:
    mov rax, 3
    mov rbx, 2
    mul rbx
...
```

# dividir

```
_start:
    mov rax, 3
    mov rbx, 2
    div rbx
...
```

# rax o al
para cada byte de rax con el valor 51 es `00 00 00 00 00 00 00 33`.
el hexadecimal 33 corresponde al caracter ascii `3` y el hexadecimal 00 corresponde a un caracte `Nulo`
debido a que el endianess es little-endian los bytes se almacenan en memoria
empezando por el byte menos significativo

```
[msg +  9 ] <- 33
[msg + 10 ] <- 00
[msg + 11 ] <- 00
[msg + 12 ] <- 00
[msg + 13 ] <- 00
[msg + 14 ] <- 00
[msg + 15 ] <- 00
[msg + 16 ] <- 00
```

```
mov rax, 3
add rax, 48
mov [msg + 9], rax
...
```
mientras con `mov [msg + 9], al` se toma el byte 33 de rax (00 00 00 00 00 00 00 33) 

```
[msg +  9 ] <- 33
```

```
mov rax, 3
add rax, 48
mov [msg + 9], al
...
```

si con `add rax, 0x38303134` se guarda los valores hexadecimales en RAX (00 00 00 00 38 30 31 34)
y se ejeuta `mov [msg +9], rax` el resultado será `numero = 4108####` 
en cambio si se ejecuta `mov [msg +9], al` imprime `numero = 4#######` 
**Nota: ** Endianess solo aplica en la memoria no en los registros

```
section .data
    msg db "numero = ########", 0ah
    len equ $ - msg

section .text
    global _start

_start: 
    add rax, 0x38303134
    mov [msg +9], rax

    mov rax, 1
    mov rdi, 1
    mov rsi, msg
    mov rdx, len
    
    syscall
    mov rax, 60
    mov rdi, 0
    syscall
```

## JUMP/Saltos
abreviaciones

`reg1` indica un registro (rax, rbx, rdi, etc)

`reg2/valor` indica que puede ir un registro ó un valor numérico 
(rax, rbx, 1, 4 ,5, etc)


### JMP ( JUMP / salto)

salta a un label

	jmp _imprimir

### JE (JUMP EQUAL  / Salto cuando es igual )

salta cuando `reg1` es igual a `reg2/valor`
	
	mov rax, 2
	cmp rax, 2
	je _imprimir


### JL (JUMP LESS  / Salto cuando es menor )

salta cuando en la comparación `reg1` es menor a `reg2/valor`  
( `cmp reg1, reg2/valor` )
	
	mov rbx, 1
	cmp rbx, 2
	jl _imprimir


### JG (JUMP EQUAL  / Salto cuando es mayor )
salta (jg _imprimir) cuando en la comparación `reg1` es mayor a 
`reg2/valor` ( `cmp reg1, reg2/valor`)

	mov rbx, 4
	cmp rbx, 2
	jg _imprimir









